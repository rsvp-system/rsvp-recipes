  Until we can roll these into a proper composer.json file, here is a quick hack for grabbing required modules.
  
  > composer require 'drupal/auto_entitylabel:^3.4'

  > composer require 'drupal/better_exposed_filters:^7.0'

  > composer require 'drupal/ckeditor_accordion:^2.2'

  > composer require 'drupal/fullcalendar:^3.0'

  > composer require 'drupal/geofield:^1.60'

  > composer require 'drupal/geocoder:^4.25'

  > composer require drupal/leaflet

  > composer require 'drupal/views_accordion:^2.0'

  