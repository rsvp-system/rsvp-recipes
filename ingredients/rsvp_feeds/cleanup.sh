#!/bin/bash
terminus drush -- config:delete feeds.feed_type.rsvp_department_import
terminus drush -- config:delete field.field.taxonomy_term.rsvp_department.feeds_item
terminus drush -- config:delete field.storage.taxonomy_term.feeds_item
terminus drush -- config:delete core.entity_view_mode.feeds_feed.full
terminus drush -- config:delete system.action.feeds_feed_clear_action
terminus drush -- config:delete system.action.feeds_feed_delete_action
terminus drush -- config:delete system.action.feeds_feed_import_action
terminus drush -- config:delete views.view.feeds_feed
terminus drush -- config:delete core.entity_view_mode.feeds_feed.token
terminus drush -- config:delete core.entity_view_mode.feeds_import_log.token
terminus drush -- config:delete core.entity_view_mode.feeds_subscription.token
terminus drush -- config:delete field.field.node.location.feeds_item
terminus drush -- config:delete field.storage.node.feeds_item
