#!/bin/bash
#drush config:delete field.field.taxonomy_term.rsvp_department.field_rsvp_dept_logo
#drush config:delete field.field.taxonomy_term.rsvp_department.field_rsvp_short_name
#drush config:delete field.field.taxonomy_term.rsvp_department.field_rsvp_url_short_name
#drush config:delete field.storage.taxonomy_term.field_rsvp_dept_logo
#drush config:delete field.storage.taxonomy_term.field_rsvp_short_name
#drush config:delete field.storage.taxonomy_term.field_rsvp_url_short_name
#drush config:delete taxonomy.vocabulary.rsvp_department
terminus drush -- config:delete field.field.taxonomy_term.rsvp_department.field_rsvp_dept_logo
terminus drush -- config:delete field.field.taxonomy_term.rsvp_department.field_rsvp_short_name
terminus drush -- config:delete field.field.taxonomy_term.rsvp_department.field_rsvp_url_short_name
terminus drush -- config:delete field.storage.taxonomy_term.field_rsvp_dept_logo
terminus drush -- config:delete field.storage.taxonomy_term.field_rsvp_short_name
terminus drush -- config:delete field.storage.taxonomy_term.field_rsvp_url_short_name
terminus drush -- config:delete taxonomy.vocabulary.rsvp_department
terminus drush -- config:delete field.field.taxonomy_term.rsvp_department.field_banner_landscape
terminus drush -- config:delete field.field.taxonomy_term.rsvp_department.field_banner_portrait
terminus drush -- config:delete field.storage.taxonomy_term.field_banner_landscape
terminus drush -- config:delete field.storage.taxonomy_term.field_banner_portrait
terminus drush -- config:delete core.entity_form_display.taxonomy_term.rsvp_department.default
terminus drush -- config:delete core.entity_view_display.taxonomy_term.rsvp_department.default
