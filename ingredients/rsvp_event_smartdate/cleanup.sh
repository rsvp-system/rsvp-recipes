#!/bin/bash
drush config:delete smart_date.smart_date_format.rsvp_event_smartdate
drush config:delete core.entity_form_display.node.rsvp_event.default
drush config:delete core.entity_view_display.node.rsvp_event.default
drush config:delete field.field.node.rsvp_event.field_rsvp_event_smartdate
drush config:delete field.storage.node.field_rsvp_event_smartdate
