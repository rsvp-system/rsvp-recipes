#!/bin/bash
../vendor/bin/drush config:delete core.entity_form_display.media.image.media_library
../vendor/bin/drush config:delete field.field.media.image.field_rsvp_media_taxonomy
../vendor/bin/drush config:delete field.storage.media.field_rsvp_media_taxonomy
../vendor/bin/drush config:delete core.entity_form_display.media.image.default
../vendor/bin/drush config:delete core.entity_view_display.media.image.full
../vendor/bin/drush config:delete core.entity_view_display.media.image.media_library
../vendor/bin/drush config:delete views.view.media